---
layout: archive
title: 
excerpt: 
tags: demo, demonstration, release, ictacademy, post
permalink: /demo/
image:
  feature: headerDemo.jpg
---

# Demonstration

Anhand der Videos kann demonstriert werden, wie Projekte funktionieren.

## Projekt 1 - Lehrstellen

Ausbildung und Berufsschule

Die Ausbildung dauert drei Jahre. Im Lehrbetrieb arbeitest du im Team mit und vertiefst dein Können in modularen Lerneinheiten und kleineren Projekten. Im zweiten Lehrjahr lernst du verschiedene Arbeitsbereiche kennen und arbeitest auch im ICT-Support. Mit überbetrieblichen Kursen und dem Besuch der Berufsfachschule erweiterst du dein theoretisches Wissen.
Ausbildungsorte sind Luzern und Zürich.

### Lehrstelle ICT Fachfrau/-mann EFZ I Story 
<iframe frameborder="0" width="480" height="360" src="https://www.youtube.com/watch?v=5YJ5RI0hN_4" allowfullscreen></iframe>


### Vidéo en français
*Arrivera bientôt.*
